package ru.aushakov.tm.command.user;

import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return TerminalConst.USER_UNLOCK_BY_LOGIN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Unlock user by login";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getUserService().unlockOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
    }

}
