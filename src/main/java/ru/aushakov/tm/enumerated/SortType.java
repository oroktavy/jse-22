package ru.aushakov.tm.enumerated;

import ru.aushakov.tm.api.entity.*;

import java.util.Comparator;

public enum SortType {

    NAME("Sort by name", Comparator.comparing(IHasName::getName)),
    STATUS("Sort by status", Comparator.comparing(IHasStatus::getStatus)),
    CREATED("Sort by created date", Comparator.comparing(IHasCreated::getCreated)),
    START_DATE("Sort by start date", Comparator.comparing(IHasStartDate::getStartDate)),
    END_DATE("Sort by end date", Comparator.comparing(IHasEndDate::getEndDate));

    private final String displayName;

    private final Comparator comparator;

    private static final SortType[] staticValues = values();

    SortType(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public static SortType toSortType(final String sortTypeId) {
        for (final SortType sortType : staticValues) {
            if (sortType.name().equalsIgnoreCase(sortTypeId)) return sortType;
        }
        return null;
    }

}
