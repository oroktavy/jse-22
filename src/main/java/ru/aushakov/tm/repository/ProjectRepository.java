package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.model.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {
}
