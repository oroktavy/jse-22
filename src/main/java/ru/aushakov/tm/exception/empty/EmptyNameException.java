package ru.aushakov.tm.exception.empty;

import ru.aushakov.tm.command.AbstractCommand;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Provided name is empty!");
    }

    public EmptyNameException(final AbstractCommand command) {
        super("Provided command '" + command.getClass().getName() + "' has empty name!");
    }

}
