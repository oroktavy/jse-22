package ru.aushakov.tm.exception.empty;

public class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Provided description is empty!");
    }

}
