package ru.aushakov.tm.exception.general;

public class NotSupportedOnServiceLayerException extends RuntimeException {

    public NotSupportedOnServiceLayerException(final String methodName) {
        super("Method " + methodName + " with such parameters is not supported for service layer call!");
    }

}
