package ru.aushakov.tm.exception.general;

public class UnknownArgumentException extends RuntimeException {

    public UnknownArgumentException(final String arg) {
        super("Argument '" + arg + "' is not supported");
    }

}
