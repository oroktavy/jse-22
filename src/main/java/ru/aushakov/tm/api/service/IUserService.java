package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.User;

public interface IUserService extends IService<User> {

    User findOneByLogin(String login);

    User removeOneByLogin(String login);

    User add(String login, String password, String email);

    void add(String login, String password, String email, String roleId);

    User setPassword(String login, String newPassword);

    User updateOneById(String id, String lastName, String firstName, String middleName);

    User updateOneByLogin(String login, String lastName, String firstName, String middleName);

    User lockOneByLogin(String login);

    User unlockOneByLogin(String login);

}
